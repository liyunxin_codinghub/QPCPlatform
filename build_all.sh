#!/bin/bash
if [ -z $1 ];then
    qmake=~/Qt5.10.1/5.10.1/gcc_64/bin/qmake
else
    qmake=$1
fi
echo "current qmake path=$qmake"
if [ ! -f $qmake ];then
    echo "qmake path not given or the path is incorrect!"
    echo "usage:./build_all /path/to/qmake"
    exit 1
fi
function compile(){
    if [ ! -d ./Release ];then
       mkdir Release
    fi
    cd Release
    $qmake ../*.pro
    make -j8
    cd ..
}
# compile Reflex,IPlugin,QtPCLViewer and TypeDataPeeker
cd ./dependency/Reflex
compile
cd ../IPlugin
compile
cd ../QtPCLViewer
compile
cd ../TypeDataPeeker
compile
cd ../../Plugins
# compile all plugins
for i in `ls .`;do
    if [ -d $i ];then
        cd $i
        compile
        cd ..
    fi
done
# compile main program
cd ..
compile
echo "build and compile completed!if the main program compile failed,"
echo "please go to QtCreator and open QPCPlatform.pro to compile main program"
echo "note:the release build path should be ./Release folder" 
